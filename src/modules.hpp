int  product(int a, int b)
{
    if ( a == 0 || b == 0) // If Either Are 0
        return 0;
    
    if ( a < 0 && b < 0) // If Both Are Negative
    {
        if (b == -1)
            return -a;
        
        else
            return -a + product(a, b+1);
    }
    
    if ( a < 0 || b < 0) // If Either Is Negative
    {
        if (a < 0) // If Left Side Is Negative
        {
            if (b == 1)
                return a;
            
            else
                return a + product(a, b-1);
        }
        
        else // If Right Side Is Negative
        {
            if (a == 1)
                return b;
            
            else
                return b + product(a-1, b);
        }
    }
    
    if (b == 1) // Normal Positive Sides
        return a;
    
    else
        return a + product(a, b-1);
}
